// Task 1

let admin, myName = "Max"; // name was changed to myName because name is deprecated

admin = myName;

console.log(admin);


// Task 2

let days = 8;

days *= 24 * 60 * 60;

console.log(days);


// Task 3

const answer = prompt("What is the capital of Montenegro?");

console.log(answer);

// або одразу виводимо відповідь в консоль
// console.log(prompt("What is the capital of Bolivia?"));
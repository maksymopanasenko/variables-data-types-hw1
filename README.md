# Variables & data types in JS

## Answers to theoretical questions

1. _Як можна оголосити змінну у Javascript?_  
> Змінні в js можна оголосити трьома способами:  
- за допомогою **var**;
- за допомогою **let**;
- за допомогою **const**;

сonst та let були введені разом із ES6 стандартом і на даний час рекомендується використовувати саме такий підхід. Вони мають блокову область видимості на відміну від var, що має глобальну область видимості. Змінна var може бути використана до того, як буде оголошення, let i const можуть бути використані тільки після ініціалізації.  

2. _У чому різниця між функцією prompt та функцією confirm?_  

Застосувавши функції confirm i prompt ми викликаємо в браузері стандартне віконце для взаємодії з користувачем.
confirm представляє користувачеві рядок тексту, який користувач може на його розсуд або підтвердити, або відхилити, цим самим передаючи як відповідь логічне "так" або "ні" відповідно.
prompt дає можливість користувачеві вводити власні дані в поле вводу і таким чином передавати їх у програму у рядковому форматі (string).  

3. _Що таке неявне перетворення типів? Наведіть один приклад._  

Неявне перетертворення типів - це автоматичний програмний перехід одного типу данних в інших під час виконнання певних процесів у скредовищі виконання коду. Наприклад, змінна str містить тип данних string, але в умові вона сприймається як логічний тип данних boolean.  

``const str = 'Hello world';
str ? console.log(str) : null``

